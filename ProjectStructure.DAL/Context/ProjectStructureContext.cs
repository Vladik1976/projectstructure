﻿using System;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities;


namespace ProjectStructure.DAL.Context
{
    public class ProjectStructureContext
    {
        private static readonly Lazy<ProjectStructureContext> lazy =
   new Lazy<ProjectStructureContext>(() => new ProjectStructureContext());

        public List<Project> Projects { get; private set; }
        public List<Task> Tasks { get; private set; }
        public List<Team> Teams { get; private set; }
        public List<User> Users { get; private set; }

        private ProjectStructureContext()
        {
            Projects = new List<Project>();
            Tasks = new List<Task>();
            Teams = new List<Team>();
            Users = new List<User>();
        }

        public static ProjectStructureContext GetInstance()
        {
            return lazy.Value;
        }
    }
}
