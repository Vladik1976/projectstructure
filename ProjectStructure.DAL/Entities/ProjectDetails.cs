﻿
namespace ProjectStructure.DAL.Entities
{
    public class ProjectDetails
    {
        public DAL.Entities.Project Project { get; set; }

        public Task LongestTask { get; set; }

        public Task ShortestTask { get; set; }

        public int NumberOfPerformers { get; set; }

    }
}