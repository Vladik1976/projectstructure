﻿using System;

namespace ProjectStructure.DAL.Entities.Abstract
{
    public abstract class BaseEntity
    {

        public BaseEntity() { }
        

        public int Id { get; set; }

    }
}
