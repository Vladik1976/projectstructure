﻿
namespace ProjectStructure.DAL.Entities
{
    public class UserDetails
    {
        public DAL.Entities.User User { get; set; }

        public DAL.Entities.Project Project { get; set; }

        public int? NumberOfTasks { get; set; }

        public int UnfinishedTasks { get; set; }

        public Task LongestTask { get; set; }
    }
}
