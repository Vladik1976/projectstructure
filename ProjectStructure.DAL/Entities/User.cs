﻿using ProjectStructure.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Entities
{
    public class User:BaseEntity
    {
        public User()
        {
            Tasks = new List<Task>();
        }
        public int? TeamId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime? RegisteredAt { get; set; }

        public DateTime? Birthday { get; set; }

        public ICollection<Task> Tasks { get; set; }
    }
}
