﻿using ProjectStructure.Client.DTOs.Project;
using ProjectStructure.Client.DTOs.Task;
using System;

namespace ProjectStructure.Client.DTOs.User
{
    public class UserDetailsDTO
    {
        public UserDTO User { get; set; }

        public ProjectDTO Project{ get; set; }
        public int? NumberOfTasks { get; set; }

        public int UnfinishedTasks { get; set; }

        public TaskDTO LongestTask { get; set; }

    }
}
