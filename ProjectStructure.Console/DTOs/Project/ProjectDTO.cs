﻿using ProjectStructure.Client.DTOs.Task;
using System;
using System.Collections.Generic;

namespace ProjectStructure.Client.DTOs.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int TeamId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? Deadline { get; set; }

        public ICollection<TaskDTO> Tasks { get; set; }
    }
}
