﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectStructure.Client.Services
{
    public class HttpService :IDisposable
    {
        private readonly HttpClient _client;
        private readonly string _endPointURL;

        public HttpService(string endPointURL)
        {
            _client = new HttpClient();
            _endPointURL = endPointURL;
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<string> GetStringAsync(string param)
        {
            return await _client.GetStringAsync(_endPointURL + param);
        }

    }

  }
