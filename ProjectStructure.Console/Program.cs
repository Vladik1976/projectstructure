﻿using Newtonsoft.Json;
using ProjectStructure.Client.DTOs.Project;
using ProjectStructure.Client.DTOs.Task;
using ProjectStructure.Client.DTOs.User;
using ProjectStructure.Client.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace ProjectStructure.Client
{
    class Program
    {
        private static HttpService _client;

        static void Main(string[] args)
        {
            _client = new HttpService(ConfigurationManager.AppSettings["EndPointURL"].ToString());

            BasicMenu();
        }

        private static async Task ProjectsByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                string response= await _client.GetStringAsync("/users/GetProjects/"+Id.ToString());
                Dictionary<ProjectDTO,int> projects = JsonConvert.DeserializeObject<Dictionary<ProjectDTO, int>>(response);

                foreach (KeyValuePair<ProjectDTO, int> keyValue in projects)
                {
                    Console.WriteLine($"Project {keyValue.Key.Name}  Tasks:{keyValue.Value.ToString()}");
                }

                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }

        private static async Task TasksByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                string response = await _client.GetStringAsync("/users/GetTasks/" + Id.ToString());
                List<TaskDTO> tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(response);

                foreach (TaskDTO t in tasks)
                {
                    Console.WriteLine($"Task Name:{t.Name}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }

        private static async Task FinishedTasksByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                string response = await _client.GetStringAsync("/users/GetFinishedTasks/" + Id.ToString());
                
                List<Tuple<int,string>> tasks = JsonConvert.DeserializeObject<List<Tuple<int,string>>>(response);

                foreach (Tuple<int, string> t in tasks)
                {
                    Console.WriteLine($"Task Id:  {t.Item1} Task Name:{t.Item2}");
                }

                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        private static async Task GetTeamsLimited()
        {
            string response = await _client.GetStringAsync("/teams/GetTeamsLimited/");

            List<Tuple<int, string, List<UserDTO>>> teams= JsonConvert.DeserializeObject<List<Tuple<int, string, List<UserDTO>>>>(response);

            foreach (Tuple<int, string, List<UserDTO>> x in teams)
            {
                Console.WriteLine($"TeamID:{x.Item1} Name:{x.Item2} Users:");
                foreach (UserDTO z in x.Item3)
                {
                    Console.WriteLine($"First Name:{z.FirstName} Last Name:{z.LastName}");
                }
            }
            BasicMenu();
        }

        private static async Task GetUsersWithTasks()
        {
            string response = await _client.GetStringAsync("/teams/GetUsersWithTasks/");

            List<Tuple<string, List<TaskDTO>>> users = JsonConvert.DeserializeObject<List<Tuple<string, List<TaskDTO>>>>(response);

            foreach (Tuple<string, List<TaskDTO>> x in users)
            {
                Console.WriteLine($"First Name:{x.Item1} Tasks:");
                foreach (TaskDTO z in x.Item2)
                {
                    Console.WriteLine($"Name:{z.Name}");
                }

            }
            BasicMenu();
        }

        private static void IncorectInput()
        {
            Console.WriteLine("Incorrect input");
            BasicMenu();
        }

        private static async Task GetUserDetails()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                string response = await _client.GetStringAsync("/teams/GetUserDetails/" + Id);

                UserDetailsDTO userDetails = JsonConvert.DeserializeObject<UserDetailsDTO>(response);

                if (userDetails.User == null)
                {
                    Console.WriteLine($"NOT Found record.");
                }
                else
                {

                    Console.WriteLine($"First Name:{userDetails.User.FirstName}");
                    Console.WriteLine($"Last Project:{userDetails.Project.Name}");
                    Console.WriteLine($"Number of Tasks in a Last Project:{userDetails.Project.Tasks.Count}");
                    Console.WriteLine($"Number of Finished Tasks :{userDetails.Project.Tasks.Count}");
                    Console.WriteLine($"Longest Task:{userDetails.LongestTask.Name}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        private static async Task GetProjectDetails()
        {
            Console.WriteLine("Project Id");
            var ProjectId = Console.ReadLine();

            if (int.TryParse(ProjectId, out int Id))
            {
                string response = await _client.GetStringAsync("/teams/GetProjectDetails/" + Id);

                ProjectDetailsDTO projectDetails = JsonConvert.DeserializeObject<ProjectDetailsDTO>(response);

                if (projectDetails.Project == null)
                {
                    Console.WriteLine($"NOT Found record.");
                }
                else
                {
                    Console.WriteLine($"Project Name: {projectDetails.Project.Name}");
                    Console.WriteLine($"Longest Task: {projectDetails.LongestTask.Name}");
                    Console.WriteLine($"Shortest Task: {projectDetails.ShortestTask.Name}");
                    Console.WriteLine($"Users involved: {projectDetails.NumberOfPerformers}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        public static void BasicMenu()
        {
            Console.WriteLine();
            Console.WriteLine("0 - Number of tasks in a project for a particular user.");
            Console.WriteLine("1 - List of tasks for a particular user.");
            Console.WriteLine("2 - Get list of tasks finished in 2021 by particular user.");
            Console.WriteLine("3 - Get list of temas contains users over 10 years old.");
            Console.WriteLine("4 - Get list of users with tasks assigned");
            Console.WriteLine("5 - Get User details");
            Console.WriteLine("6 - Get Project Details");

            var input = Console.ReadKey();
            switch (input.KeyChar)
            {
                case '0':
                    ProjectsByUser().Wait();
                    break;
                case '1':
                    TasksByUser().Wait();
                    break;
                case '2':
                    FinishedTasksByUser().Wait();
                    break;
                case '3':
                    GetTeamsLimited().Wait();
                    break;
                case '4':
                    GetUsersWithTasks().Wait();
                    break;
                case '5':
                    GetUserDetails().Wait();
                    break;
                case '6':
                    GetProjectDetails().Wait();
                    break;
                default:
                    IncorectInput();
                    break;
            }
        }
       
    }
}
