﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.BLL.DTOs.Task;
using ProjectStructure.BLL.DTOs.User;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/users/[action]")]
    [ApiController]
    public class UserControler : ControllerBase
    {
        private protected readonly IMapper _mapper;
        private readonly UserService _userService;

        public UserControler(UserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [Route("{id}")]
        [HttpGet]
        public Dictionary<ProjectDTO, int> GetProjects(int id)
        {
            var projects = _userService.GetProjects(id);
            Dictionary<ProjectDTO,int> projectsDTO = _mapper.Map<Dictionary<Project,int>, Dictionary<ProjectDTO,int>>(projects);
           
            return projectsDTO;
        }

        [Route("{id}")]
        [HttpGet]
        public List<TaskDTO> GetTasks(int id)
        {

            var tasks= _userService.GetTasks(id);
            List<TaskDTO> tasksDTO = _mapper.Map<List<Task>, List<TaskDTO>>(tasks);

            return tasksDTO;
        }

        [Route("{id}")]
        [HttpGet]
        public List<Tuple<int, string>> GetFinishedTasks(int id)
        {
            return _userService.GetFinishedTasks(id);
        }

        [HttpGet]
        public List<Tuple<string, List<TaskDTO>>> GetUsersWithTasks()
        {
            var users= _userService.GetUsersWithTasks();
            List<Tuple<string, List<TaskDTO>>> tasksDTO = _mapper.Map<List<Tuple<string, List<Task>>>, List<Tuple<string, List<TaskDTO>>>>(users);

            return tasksDTO;
        }

        [Route("{id}")]
        [HttpGet]
        public UserDetailsDTO GetUserDetails(int id)
        {
            var users = _userService.GetUserDetails(id);
            UserDetailsDTO userDetailsDTO = _mapper.Map<UserDetailsDTO>(users);

            return userDetailsDTO;
        }
    }
}
