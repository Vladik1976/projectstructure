﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTOs.Team;
using ProjectStructure.BLL.DTOs.User;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/teams/[action]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private protected readonly IMapper _mapper;
        private readonly TeamService _teamService;
        public TeamController(TeamService teamService, IMapper mapper)
        {
            _teamService = teamService;
            _mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<Team> Get()
        {
            return _teamService.Get();
        }

        [HttpGet]
        public List<Tuple<int,string, List<UserDTO>>> GetTeamsLimited()
        {
            List <Tuple<int, string, List<User>>> teams= _teamService.GetTeamsLimited();

            List<Tuple<int, string, List<UserDTO>>> teamsDTO = _mapper.Map<List<Tuple<int, string, List<User>>>, List<Tuple<int, string, List<UserDTO>>>>(teams);
            return teamsDTO;
        }

    }
}
