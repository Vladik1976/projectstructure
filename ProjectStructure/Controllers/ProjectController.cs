﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.DAL.Entities;
using AutoMapper;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/projects/[action]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private protected readonly IMapper _mapper;
        private readonly ProjectService _projectService;
        public ProjectController(ProjectService projectService, IMapper mapper)
        {
            _projectService = projectService;
            _mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<Project> Get()
        {
            return _projectService.Get();
        }

        [Route("{id}")]
        [HttpGet]
        public ProjectDetailsDTO GetProjectDetails(int id)
        {
            ProjectDetails projectDetails=_projectService.GetProjectDetails(id);
            ProjectDetailsDTO projectDetailsDTO = _mapper.Map<ProjectDetailsDTO>(projectDetails);

            return projectDetailsDTO;
        }

    }
}