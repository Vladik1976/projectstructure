﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Repositories
{
    class TaskRepository
    {
        private ProjectStructureContext _context;

        public TaskRepository(ProjectStructureContext context)
        {
            _context = context;
        }

        public IEnumerable<Task> GetAll()
        {
            return _context.Tasks;
        }

        public Task Get(int id)
        {
            return _context.Tasks.FirstOrDefault(t => t.Id == id);
        }

        public void Create(Task task)
        {
            _context.Tasks.Add(task);
        }

        public void Update(Task task)
        {
            Task taskToUpdate = _context.Tasks.FirstOrDefault(t => t.Id == task.Id);
            if (taskToUpdate != null)
            {
                taskToUpdate.Name = task.Name;
                taskToUpdate.Description = task.Description;
                taskToUpdate.FinishedAt = task.FinishedAt;
                taskToUpdate.PerformerId = task.PerformerId;
                taskToUpdate.State = task.State;
                taskToUpdate.ProjectId = task.ProjectId;
            }

        }

        public void Delete(int id)
        {
            Task task = _context.Tasks.FirstOrDefault(t => t.Id == id);
            if (task != null)
            {
                _context.Tasks.Remove(task);
            }
        }
    }
}
