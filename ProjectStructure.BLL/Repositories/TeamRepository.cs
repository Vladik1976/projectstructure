﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Repositories
{
    class TeamRepository
    {
        private ProjectStructureContext _context;

        public TeamRepository(ProjectStructureContext context)
        {
            _context = context;
        }

        public IEnumerable<Team> GetAll()
        {
            return _context.Teams.GroupJoin(_context.Users,
               t => t.Id,
               u => u.TeamId,
               (t, users) => new Team
               {
                   Id = t.Id,
                   Name = t.Name,
                   CreatedAt = t.CreatedAt,
                   Users = users.ToList()
               }).ToList();
        }

        public Team Get(int id)
        {
            return GetAll().FirstOrDefault(t => t.Id == id);
        }

        public void Create(Team team)
        {
            _context.Teams.Add(team);
        }

        public void Update(Team team)
        {
            Team teamToUpdate = _context.Teams.FirstOrDefault(t => t.Id == team.Id);
            if (teamToUpdate != null)
            {
                teamToUpdate.Name = team.Name;
            }

        }

        public void Delete(int id)
        {
            Team team = _context.Teams.FirstOrDefault(t => t.Id == id);
            if (team != null)
            {
                _context.Teams.Remove(team);
            }
        }
    }
}
