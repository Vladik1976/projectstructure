﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Repositories 
{
    class UnitOfWork : IDisposable
    {
        private ProjectStructureContext _context = ProjectStructureContext.GetInstance();
        private ProjectRepository _projectRepository;
        private TaskRepository _taskRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;

        public ProjectRepository Projects
        {
            get
            {
                if (_projectRepository == null)
                    _projectRepository = new ProjectRepository(_context);
                return _projectRepository;
            }
        }

        public TaskRepository Tasks
        {
            get
            {
                if (_taskRepository == null)
                    _taskRepository = new TaskRepository(_context);
                return _taskRepository;
            }
        }

        public TeamRepository Teams
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository(_context);
                return _teamRepository;
            }
        }

        public UserRepository Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_context);
                return _userRepository;
            }
        }



        public void Dispose()
        {

        }
    }
}
