﻿using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repositories
{
    class ProjectRepository
    {
        private ProjectStructureContext _context;

        public ProjectRepository(ProjectStructureContext context)
        {
            _context = context;
        }

        public IEnumerable<Project> GetAll()
        {
            return _context.Projects.GroupJoin(_context.Tasks,
                p => p.Id,
                t => t.ProjectId,
                (p, tasks) => new Project
                {
                    Id = p.Id,
                    AuthorId = p.AuthorId,
                    Name = p.Name,
                    Description = p.Description,
                    CreatedAt = p.CreatedAt,
                    Deadline = p.Deadline,
                    Tasks = (from t in _context.Tasks
                             select new Task
                             {
                                 ProjectId = t.ProjectId,
                                 PerformerId = t.PerformerId,
                                 Performer = (from u in _context.Users.GroupJoin(_context.Tasks,
                                                u => u.Id,
                                                t => t.PerformerId,
                                                (u, tasks) => new User
                                                {
                                                    Id = u.Id,
                                                    TeamId = u.TeamId,
                                                    FirstName = u.FirstName,
                                                    LastName = u.LastName,
                                                    Email = u.Email,
                                                    RegisteredAt = u.RegisteredAt,
                                                    Birthday = u.Birthday,
                                                    Tasks = tasks.ToList()
                                                })
                                                where u.Id == t.PerformerId
                                              select (u)).FirstOrDefault(),
                                 Name = t.Name,
                                 Description = t.Description,
                                 State = t.State,
                                 CreatedAt = t.CreatedAt,
                                 FinishedAt = t.FinishedAt

                             }).ToList(),
                    Author = (from u in _context.Users.GroupJoin(_context.Tasks,
                                                u => u.Id,
                                                t => t.PerformerId,
                                                (u, tasks) => new User
                                                {
                                                    Id = u.Id,
                                                    TeamId = u.TeamId,
                                                    FirstName = u.FirstName,
                                                    LastName = u.LastName,
                                                    Email = u.Email,
                                                    RegisteredAt = u.RegisteredAt,
                                                    Birthday = u.Birthday,
                                                    Tasks = tasks.ToList()
                                                })
                              where u.Id == p.AuthorId
                              select (u)).FirstOrDefault(),
                    TeamId = p.TeamId,
                    Team = (from t in _context.Teams.GroupJoin(_context.Users,
                                   t => t.Id,
                                   u => u.TeamId,
                                   (t, users) => new Team
                                   {
                                       Id = t.Id,
                                       Name = t.Name,
                                       CreatedAt = t.CreatedAt,
                                       Users = users.ToList()
                                   })
                            where t.Id == p.Id
                            select (t)).FirstOrDefault()

                }).ToList();

        }

        public Project Get(int id)
        {
            return _context.Projects.FirstOrDefault(p => p.Id == id);
        }

        public void Create(Project project)
        {
            _context.Projects.Add(project);
        }

        public void Update(Project project)
        {
            Project projectToUpdate = _context.Projects.FirstOrDefault(p=>p.Id==project.Id);
            if(projectToUpdate!=null)
            {
                projectToUpdate.Name = project.Name;
                projectToUpdate.Description = project.Description;
                projectToUpdate.Deadline = project.Deadline;
                projectToUpdate.TeamId = project.TeamId;
            }

        }

        public void Delete(int id)
        {
            Project project = _context.Projects.FirstOrDefault(p => p.Id == id);
            if (project != null)
            {
                _context.Projects.Remove(project);
            }
        }
    }
}
