﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.BLL.Repositories
{
    class UserRepository
    {
        private ProjectStructureContext _context;

        public UserRepository(ProjectStructureContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users.GroupJoin(_context.Tasks,
               u => u.Id,
               t => t.PerformerId,
               (u, tasks) => new User
               {
                   Id = u.Id,
                   TeamId = u.TeamId,
                   FirstName = u.FirstName,
                   LastName = u.LastName,
                   Email = u.Email,
                   RegisteredAt = u.RegisteredAt,
                   Birthday = u.Birthday,
                   Tasks = tasks.ToList()
               }).ToList();
         }

        public User Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public void Create(User user)
        {
            _context.Users.Add(user);
        }

        public void Update(User user)
        {
            User userToUpdate = _context.Users.FirstOrDefault(u => u.Id == user.Id);
            if (userToUpdate != null)
            {
                userToUpdate.TeamId = user.TeamId;
                userToUpdate.FirstName = user.FirstName;
                userToUpdate.LastName = user.LastName;
                userToUpdate.Email = user.Email;
                userToUpdate.Birthday = user.Birthday;
            }

        }

        public void Delete(int id)
        {
            User user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user != null)
            {
                _context.Users.Remove(user);
            }
        }
    }
}
