﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<ProjectDTO, Project>();

            CreateMap<ProjectCreateDTO, Project>();

            CreateMap<Project, ProjectCreateDTO>();

            CreateMap<ProjectDetails, ProjectDetailsDTO>();

            CreateMap<ProjectDetailsDTO, ProjectDetails>();
        }
    }
}
