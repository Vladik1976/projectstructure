﻿
using AutoMapper;
using ProjectStructure.BLL.DTOs.User;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();

            CreateMap<UserDTO, User>();

            CreateMap<UserCreateDTO, User>();

            CreateMap<User, UserCreateDTO>();

            CreateMap<UserDetails, UserDetailsDTO>();

            CreateMap<UserDetailsDTO, UserDetails>();
        }
    }
}
