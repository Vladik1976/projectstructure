﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Team;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<TeamDTO, Team>();

            CreateMap<TeamCreateDTO, Team>();

            CreateMap<Team, TeamCreateDTO>();
        }
    }
}