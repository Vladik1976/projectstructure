﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Task;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();

            CreateMap<TaskDTO, Task>();
        }
    }
}
