﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Team;
using ProjectStructure.BLL.Repositories;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TeamService :BaseService
    {
        private readonly UnitOfWork _unitOfWork;
        public TeamService(IMapper mapper) : base(mapper) 
        {
            _unitOfWork = new UnitOfWork();
        }
        /// <summary>
        /// Get list of teams contains user over 10 years old
        /// </summary>
        /// <returns></returns>
        public List<Tuple<int,string,List<User>>> GetTeamsLimited()
        {
            var tasks = (from t in _unitOfWork.Teams.GetAll()
                         select new { t.Id, t.Name, Users = t.Users.OrderByDescending(u=>u.RegisteredAt).ToList() }).Where(t=>t.Users.All(u=> DateTime.Now.Year - u.Birthday.Value.Year>10))
                        .Select(t => new Tuple<int, string, List<User>>(t.Id, t.Name, t.Users)).ToList();
                        

            return tasks;

        }

        public IEnumerable<Team> Get()
        {
            return _unitOfWork.Teams.GetAll();
        }
        public void Create(TeamCreateDTO teamDTO)
        {
            var teamEntity = _mapper.Map<Team>(teamDTO);
            _unitOfWork.Teams.Create(teamEntity);
        }
        public void Update(TeamDTO teamDTO)
        {
            var teamEntity = _mapper.Map<Team>(teamDTO);
            _unitOfWork.Teams.Update(teamEntity);
        }

        public void Delete(int id)
        {
            _unitOfWork.Teams.Delete(id);
        }
    }
}
