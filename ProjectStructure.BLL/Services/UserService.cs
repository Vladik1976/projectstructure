﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.User;
using ProjectStructure.BLL.Repositories;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class UserService :BaseService
    {
        private readonly UnitOfWork _unitOfWork;

        public UserService(IMapper mapper) : base(mapper) 
        {
            _unitOfWork = new UnitOfWork();
        }

        ///<summary>Get list of projects where at least one task assigned to the particular used
        ///Returns Disctionary</summary>
        ///
        public Dictionary<Project, int> GetProjects(int userId)
        {
            return _unitOfWork.Projects.GetAll().Where(p => p.Tasks.Any(t => t.PerformerId == userId)).ToDictionary(p => p, p => p.Tasks.Where(t => t.PerformerId == userId).Count());
        }

        ///<summary>Get a list of tasks assigned to the particular user and which name length less than 45 chars</summary>
        ///
        public List<Task> GetTasks(int userId)
        {
            var tasks = _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == userId && t.Name.Length < 45).ToList();

            return tasks;
        }

        ///<summary>Get a list of tasks finished in 2021 by particular user</summary>
        ///
        public List<Tuple<int,string>> GetFinishedTasks(int userId)
        {
            var tasks = _unitOfWork.Tasks.GetAll().Where(t => t.FinishedAt != null).Where(t => t.FinishedAt.Value.Year == 2021 && t.PerformerId == userId)
                .Select(t => new { t.Id, t.Name }).AsEnumerable()
                .Select(t => new Tuple<int, string>(t.Id, t.Name)).ToList();
            
            return tasks;
        }
        /// <summary>
        /// Returns Users with associated tasks
        /// </summary>
        /// <returns></returns>
        public List<Tuple<string,List<Task>>> GetUsersWithTasks()
        {
            var users = (from u in _unitOfWork.Users.GetAll()
                         select new { u.FirstName, Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToList() }
                        ).Select(u => new Tuple<string, List<Task>>(u.FirstName, u.Tasks)).OrderBy(u => u.Item1).ToList();


            return users;
        }
        public UserDetails GetUserDetails (int userId)
        {
            var user = (from u in _unitOfWork.Users.GetAll()
                        let projects= (from p in _unitOfWork.Projects.GetAll()
                                       where p.Tasks.ToList().Any(t => t.PerformerId == u.Id)
                                       select (p))
                        where u.Id == userId
                        select new UserDetails
                        {
                            User = u,
                            Project = projects.OrderByDescending(x => x.CreatedAt).FirstOrDefault(),

                            NumberOfTasks = projects.OrderByDescending(x => x.CreatedAt).FirstOrDefault()?.Tasks.Count,

                            UnfinishedTasks = (from t in u.Tasks
                                               where t.PerformerId == u.Id && t.FinishedAt == null
                                               select (t)).Count(),

                            LongestTask = (from t in u.Tasks
                                           orderby (t.FinishedAt ?? DateTime.Now) - t.CreatedAt descending
                                           where t.PerformerId == u.Id
                                           select (t)).FirstOrDefault()
                        }).FirstOrDefault();

            return user;

        }
        public void Create(UserCreateDTO userCreateDTO)
        {
            var userEntity = _mapper.Map<User>(userCreateDTO);
            userEntity.RegisteredAt = System.DateTime.Now;
            _unitOfWork.Users.Create(userEntity);
        }
        public IEnumerable<User>Get()
        {
            return _unitOfWork.Users.GetAll();
        }

        public void Update (UserDTO userDTO)
        {
            var userEntity = _mapper.Map<User>(userDTO);
            _unitOfWork.Users.Update(userEntity);
        }

        public void Delete(int id)
        {
            _unitOfWork.Users.Delete(id);
        }
    }
}
