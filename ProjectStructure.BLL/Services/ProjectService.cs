﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.BLL.Repositories;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;
using System.Linq;


namespace ProjectStructure.BLL.Services
{
    public class ProjectService: BaseService
    {
        private readonly UnitOfWork _unitOfWork;
        public ProjectService(IMapper mapper) : base(mapper) 
        {
            _unitOfWork = new UnitOfWork();
        }
        public ProjectDetails GetProjectDetails(int projectId)
        {
            var projectdetails = (from p in _unitOfWork.Projects.GetAll()
                                  where p.Id == projectId
                        select new ProjectDetails
                        {
                            Project = p,

                            LongestTask = p.Tasks.OrderByDescending(x=>x.Description).FirstOrDefault(),

                            ShortestTask = p.Tasks.OrderBy(x=>x.Name).FirstOrDefault(),

                            NumberOfPerformers = (from t in p.Tasks
                                                  where p.Description.Length>20 || p.Tasks.Count<3
                                                  select t.PerformerId).Distinct().Count()
                        }).FirstOrDefault();

            return projectdetails;

        }
        public IEnumerable<Project> Get() {
            return _unitOfWork.Projects.GetAll();
        }
        public void Create(ProjectCreateDTO projectDTO)
        {
            var projectEntity = _mapper.Map<Project>(projectDTO);
            projectEntity.CreatedAt = System.DateTime.Now;
            _unitOfWork.Projects.Create(projectEntity);
        }
        public void Update(ProjectDTO projectDTO)
        {
            var projectEntity = _mapper.Map<Project>(projectDTO);
            _unitOfWork.Projects.Update(projectEntity);
        }

        public void Delete(int id)
        {
            _unitOfWork.Projects.Delete(id);
        }



    }
}
