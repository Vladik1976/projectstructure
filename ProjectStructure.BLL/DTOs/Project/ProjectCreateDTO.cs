﻿using System;

namespace ProjectStructure.BLL.DTOs.Project
{
    public class ProjectCreateDTO
    {
        public int AuthorId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int TeamId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? Deadline { get; set; }

    }
}
