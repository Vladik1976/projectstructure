﻿using System;
using ProjectStructure.BLL.DTOs.Task;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.DTOs.Project
{
    public class ProjectDetailsDTO
    {
        public ProjectDetailsDTO Project { get; set; }

        public TaskDTO LongestTask { get; set; }

        public TaskDTO ShortestTask { get; set; }

        public int NumberOfPerformers { get; set; }

    }
}