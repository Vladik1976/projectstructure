﻿
namespace ProjectStructure.BLL.DTOs.Team
{
    public class TeamDTO
    {
        public string Name { get; set; }

        public int Id { get; private set; }
    }
}
