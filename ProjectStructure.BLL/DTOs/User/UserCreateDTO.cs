﻿using System;

namespace ProjectStructure.BLL.DTOs.User
{
    public class UserCreateDTO
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime? Birthday { get; set; }
    }
}
