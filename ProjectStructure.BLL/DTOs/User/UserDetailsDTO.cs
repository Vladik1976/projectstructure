﻿using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.BLL.DTOs.Task;
using ProjectStructure.DAL.Entities;
using System;

namespace ProjectStructure.BLL.DTOs.User
{
    public class UserDetailsDTO
    {
        public UserDTO User { get; set; }

        public ProjectDTO Project { get; set; }

        public int? NumberOfTasks { get; set; }

        public int UnfinishedTasks { get; set; }

        public TaskDTO LongestTask { get; set; }
    }
}
